
public class MemInfoHelper
{
	public static String memInfo()
	{
		return String.format(
			"free: %1$,014d Byte allocated: %2$,014d Byte total: %3$,014d Byte",
			Runtime.getRuntime().freeMemory(),
			Runtime.getRuntime().totalMemory(),
			Runtime.getRuntime().maxMemory()
		);
	}
}
