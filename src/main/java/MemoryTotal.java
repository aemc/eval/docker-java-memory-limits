
public class MemoryTotal
{
	public static <E> void main( String[] args )
	{
		System.out.println(
				String.format(
					"Memory-Total: %1$,014d Byte",
					Runtime.getRuntime().maxMemory()
				)
		);
	}
}
