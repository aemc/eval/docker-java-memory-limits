
import java.util.ArrayList;

public class MemoryEater
{
	public static <E> void main( String[] args )
	{
		System.out.println( MemInfoHelper.memInfo() );
		
		ArrayList<byte[]> list = new ArrayList<byte[]>();
		
		while( true )
		{
			byte[] bytes = new byte[ 1024 * 1024 * 16 ];
			list.add( bytes );
			System.out.println( MemInfoHelper.memInfo() );
		}
	}
}
