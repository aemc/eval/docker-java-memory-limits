FROM openjdk:8u131-jdk-alpine

ADD ./target/eval-docker-java.jar /opt/eval-docker-java.jar

WORKDIR /opt/
ENTRYPOINT [ "java", "-cp", "eval-docker-java.jar" ]